"""
Fetch updated site log files from remote HTTPS sites and upload them to GWS incoming site log bucket.
"""

import logging
import datetime
import os
import re
import shutil
import tempfile

import boto3
import dateutil.parser
import requests

from bs4 import BeautifulSoup

logger = logging.getLogger(__name__) # pylint: disable=invalid-name
logger.setLevel(logging.INFO)

class SiteLogHttpsSource(object):
    def __init__(self, source):
        self.url = source

site_log_https_sources = [ # pylint: disable=invalid-name
    SiteLogHttpsSource('https://data.geonet.org.nz/gnss/sitelogs/logs/'),
    SiteLogHttpsSource('https://files.igs.org/pub/station/log_9char/')
]

def parse_date(string):
    """
    Given date in format 'yyyymmdd', return python date(yyyy, mm, dd)
    """
    return datetime.date(int(string[0:4]), int(string[4:6]), int(string[6:8]))

def parse_site_log_file_name(file_name):
    """
    Given site log file name in format 'abcd(mmccc)?_yyyymmdd.log', return ('abcd', date(yyyy, mm, dd))
    """
    underscore = file_name.index('_')
    return file_name[0:4], parse_date(file_name[underscore+1:underscore+9])

class HttpsLogHunter(object):
    site_log_file_name_pattern = re.compile(r'(?P<file_name>\w{4}(\d{2}\w{3})?_\d{8}\.log)', re.IGNORECASE)

    def __init__(self, https_sources, current_site_logs):
        self.https_sources = https_sources
        self.current_site_logs = current_site_logs

    def with_site_log_updates(self, update_handler):
        download_directory_name = tempfile.mkdtemp()

        for _four_char_id, (https, remote_file_name) in self.get_site_log_updates().items():
            downloaded_file_name = self.download(https, remote_file_name, download_directory_name)
            logger.info('Downloaded ' + remote_file_name + ' from ' + https.url)
            try:
                with open(downloaded_file_name, 'rb') as downloaded_file:
                    update_handler(downloaded_file)
            except Exception as err: #pylint: disable=broad-except
                logger.error(err, exc_info=True)

        if not os.listdir(download_directory_name):
            logger.info('No updates found')

        shutil.rmtree(download_directory_name)

    def download(self, https, file_name, directory):
        download_path = os.path.join(directory, file_name)
        site_log_file = open(download_path, 'w')
        response = requests.get(https.url + file_name)
        site_log_file.writelines(response.text)
        site_log_file.close()
        return site_log_file.name

    def get_site_log_updates(self):
        remote_file_names = []
        for https in self.https_sources:
            logger.info('Fetching site log listing from ' + https.url)
            response = requests.get(https.url)
            soup = BeautifulSoup(response.text, 'html.parser')
            for url in soup.find_all('a'):
                link = str(url.get('href'));
                if link.endswith(".log"):
                    match = re.search(type(self).site_log_file_name_pattern, link)
                    if match:
                        remote_file_names.append((https, match.group('file_name')))

        remote_site_logs = {}

        for remote_file_name in remote_file_names:
            four_char_id, date_prepared = parse_site_log_file_name(remote_file_name[1])

            if four_char_id in self.current_site_logs:
                if not self.current_site_logs[four_char_id] or date_prepared > self.current_site_logs[four_char_id]:
                    remote_site_logs[four_char_id] = (remote_file_name[0], remote_file_name[1])

        return remote_site_logs

def gws_list_site_logs():
    site_logs = {}

    gws_url = os.environ['gws_url']
    pageRequest = gws_url + '/siteLogs?projection=datePrepared&size=500'

    while True:
        response = requests.get(pageRequest)
        response.raise_for_status()

        for site_log in response.json()['_embedded']['siteLogs']:
            datePrepared = site_log['datePrepared']
            site_logs[site_log['fourCharacterId'].lower()] = dateutil.parser.parse(datePrepared).date() if datePrepared else None

        links = response.json()['_links']
        if 'next' not in links:
            break

        pageRequest = links['next']['href'] + '&projection=datePrepared'

    return site_logs

def lambda_handler(event, context):
    gws_logs = gws_list_site_logs()
    https_log_hunter = HttpsLogHunter(site_log_https_sources, gws_logs)

    s3 = boto3.client('s3')
    incoming_bucket_name = os.environ['incoming_bucket_name']

    def upload_site_log(site_log_file, bucket_name):
        s3.put_object(
            Bucket=bucket_name,
            Key=os.path.split(site_log_file.name)[1],
            Body=site_log_file)

        logger.info('Uploaded ' + os.path.split(site_log_file.name)[1] + ' to s3://' + bucket_name)

    https_log_hunter.with_site_log_updates(lambda site_log_file: upload_site_log(site_log_file, incoming_bucket_name))

if __name__ == '__main__':
    loggerStreamHandler = logging.StreamHandler()
    loggerStreamHandler.setLevel(logging.INFO)
    logger.addHandler(loggerStreamHandler)
    lambda_handler(None, None)

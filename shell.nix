{ pkgs ? import ./nix {} }:

let
  projectName = "geodesyml-converter";

  env = pkgs.buildEnv {
    name = projectName + "-env";
    paths = with pkgs; [
      awscli2
      bash
      cacert
      coreutils
      docker
      git
      gnssCommon.parameters
      jq
      niv
      nix
      python39Packages.virtualenv
      terraform_0_12
      wget
      zip
    ];
  };

in
  pkgs.mkShell {

    buildInputs = [
      env
    ];

    shellHook = ''
      export AWS_REGION=$AWS_DEFAULT_REGION
      export PROJECT_NAME=${projectName}
      export SSL_CERT_FILE="${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"

      if [[ $IN_NIX_SHELL = pure ]]; then
          unset NIX_SSL_CERT_FILE
      fi

      unset SOURCE_DATE_EPOCH

      if [ -d python3-env ]; then
        . python3-env/bin/activate
      fi

      if [ -e "./aws/aws-env.sh" ]; then
        . ./aws/aws-env.sh
      fi
    '';
  }

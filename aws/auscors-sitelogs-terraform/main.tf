provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
  }
}

module "s3" {
  source = "./modules/s3/"

  submitter_arns = var.submitter_arns

  environment = var.environment
  application = var.application
  owner       = var.owner
}

module "lambda" {
  source = "./modules/lambda/"

  sns_arn                     = var.sns_arn
  incoming_bucket_name        = module.s3.incoming_bucket_name
  incoming_bucket_arn         = module.s3.incoming_bucket_arn
  bucket_name                 = module.s3.bucket_name
  site_log_egress_bucket      = module.s3.site_log_egress_bucket
  cognito_admin_auth_role_arn = var.cognito_admin_auth_role_arn
  cognito_user_pool_id        = var.cognito_user_pool_id
  cognito_user_pool_client_id = var.cognito_user_pool_client_id
  gws_url                     = var.gws_url
  parameter_store_role_arn    = var.parameter_store_role_arn
  environment                 = var.environment
  application                 = var.application
  owner                       = var.owner
}

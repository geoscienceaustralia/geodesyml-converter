variable "region" {
  default = "ap-southeast-2"
}

variable "tf_state_bucket" {
  description = "Name of terraform state S3 bucket."
  default     = "auscors-terraform-state-dev"
}

variable "tf_state_table" {
  description = "Name of terraform state lock DDB table."
  default     = "auscors-terraform-state-dev"
}

variable "environment" {
  description = "Deployment environment. Suffixes most created objects."
  default     = "dev"
}

variable "application" {
  description = "Application name. Used to prefix most created objects, including terraform state file."
  default     = "auscors-sitelogs"
}

variable "owner" {
  description = "Application owner."
  default     = "Geodesy Operations"
}

variable "sns_arn" {
  description = "ARN of SNS topic which converter Lambda is invoked by."
  default     = "arn:aws:sns:ap-southeast-2:094928090547:DevGeodesy-SiteLogReceived-EYB2P4K966EE"
}

variable "cognito_admin_auth_role_arn" {
  description = "Cognito admin authoriser role arn"
}

variable "cognito_user_pool_id" {
  description = "Cognito user pool id"
}

variable "cognito_user_pool_client_id" {
  description = "Cognito user pool client id"
}

variable "gws_url" {
  description = "Geodesy Web Services URL for Lambda to retrieve GeodesyML from."
  default     = "https://dev-metadata.gnss.ga.gov.au/api"
}

variable "submitter_arns" {
  description = "ARNs for users who can submit text site logs to the incoming bucket"
  type        = list(string)
  default     = ["arn:aws:iam::688660191997:root"]
}

variable "parameter_store_role_arn" {
  type = string
}

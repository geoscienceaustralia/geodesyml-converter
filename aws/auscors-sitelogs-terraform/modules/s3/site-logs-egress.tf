locals {
  site_log_egress_bucket_name = format(
    "ga-gnss-metadata-site-logs%s", var.environment == "prod" ? "" : "-${var.environment}"
  )
}

resource "aws_s3_bucket" "site_log_egress" {
  bucket = local.site_log_egress_bucket_name

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_public_access_block" "site_log_egress" {
  bucket              = aws_s3_bucket.site_log_egress.id
  block_public_policy = false
}

resource "aws_s3_bucket_policy" "site_log_egress" {
  bucket = aws_s3_bucket.site_log_egress.id

  policy = jsonencode(
    {
      Version = "2012-10-17"
      Statement = [
        {
          Effect    = "Allow"
          Principal = "*"
          Action = [
            "s3:ListBucket",
          ],
          Resource : [
            aws_s3_bucket.site_log_egress.arn,
          ]
        },
        {
          Effect    = "Allow"
          Principal = "*"
          Action = [
            "s3:GetObject",
          ],
          Resource : [
            "${aws_s3_bucket.site_log_egress.arn}/*"
          ]
        }
      ]
    }
  )
}

output "site_log_egress_bucket" {
  value = {
    arn  = aws_s3_bucket.site_log_egress.arn
    name = aws_s3_bucket.site_log_egress.id
  }
}

# AWS Variables
variable "region" {
  default = "ap-southeast-2"
}

#Tags
variable "application" {
}

variable "owner" {
}

variable "environment" {
}

variable "submitter_arns" {
  type = list(string)
}

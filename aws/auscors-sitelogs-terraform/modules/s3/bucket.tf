resource "aws_s3_bucket" "incoming_bucket" {
  bucket = "${var.application}-incoming-${var.environment}"

  lifecycle {
    # Will change to true before prod deployment
    prevent_destroy = false
  }

  versioning {
    enabled = true
  }

  tags = {
    environment = var.environment
    application = var.application
    owner       = var.owner
  }
}

output "incoming_bucket_name" {
  value = aws_s3_bucket.incoming_bucket.id
}

output "incoming_bucket_arn" {
  value = aws_s3_bucket.incoming_bucket.arn
}

resource "aws_s3_bucket" "data_bucket" {
  bucket = "${var.application}-converted-${var.environment}"
  acl    = "public-read"

  lifecycle {
    # Will change to true before prod deployment
    prevent_destroy = false
  }

  versioning {
    enabled = true
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }

  tags = {
    environment = var.environment
    application = var.application
    owner       = var.owner
  }

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": "*",
      "Action": [
        "s3:ListBucket", 
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::${var.application}-converted-${var.environment}",
        "arn:aws:s3:::${var.application}-converted-${var.environment}/*"
      ]
    }
  ]
}
POLICY
}

output "bucket_name" {
  value = aws_s3_bucket.data_bucket.id
}

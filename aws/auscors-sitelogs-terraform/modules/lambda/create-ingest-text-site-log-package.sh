#!/usr/bin/env bash

set -euo pipefail

scriptDir=$(readlink -f "${BASH_SOURCE[0]%/*}")
sourceDir=$(readlink -f "$scriptDir"/../../../..)

if [ "${VIRTUAL_ENV:-}" == "" ]; then
    virtualenv "$sourceDir"/python3-env
    . "$sourceDir"/python3-env/bin/activate
    (cd "$sourceDir" && pip install -r requirements.txt)
    (cd "$sourceDir" && pip install .)
fi

pip install requests==2.28.1 boto3==1.24.46

sitePackages=$(pip show requests | grep ^Location: | cut -f2 -d: | sed 's/^ //')

(cd "$sitePackages" && zip "$scriptDir"/ingest_text_site_log_lambda.zip -r \
    requests \
    urllib3 \
    pyxb \
    charset_normalizer \
    idna \
    certifi \
    iso3166 \
    SiteLogToGeodesyML)

(cd "$scriptDir" && zip ingest_text_site_log_lambda.zip -r ingest_text_site_log_lambda.py)

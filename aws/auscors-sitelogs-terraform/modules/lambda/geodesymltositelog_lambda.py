import os
import json
import urllib.request
import logging
import boto3
from boto3.session import Session
from io import BytesIO

from GeodesyMLToSiteLog import geodesymltositelog

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
    output_bucket_name = os.environ['output_bucket_name']
    site_log_egress_bucket_name = os.environ['site_log_egress_bucket_name']
    gws_url = os.environ['gws_url']

    try:
        sns_message = json.loads(event['Records'][0]['Sns']['Message'])

        logger.info('SiteLogReceived event for fourCharacterId {}'.format(
            sns_message['fourCharacterId']))

    except:
        #except KeyError, IndexError, ValueError:
        logger.error('Malformed Event or SNS Message object:\n{}'.format(event))
        raise

    try:
        request_url = '{}/siteLogs/search/findByFourCharacterId?id={}'.format(
            gws_url, sns_message['fourCharacterId'])
        request = urllib.request.Request(request_url, headers={'Accept': 'application/xml'})
        xml_string = urllib.request.urlopen(request).read()

    except:
        logger.error('Failed to get GeodesyML document for {} from GWS API'.format(
            sns_message['fourCharacterId']))
        raise

    try:
        site_log_filename_1, site_log_data_1 = geodesymltositelog.parseXML(xml_string, "1")
        site_log_filename_2, site_log_data_2 = geodesymltositelog.parseXML(xml_string, "2")

    except Exception as err:
        # There is currently no error handling in the xml2log module, so expect any exception
        logger.error('Failed to parse XML to site log for {}:\n{}\n{}\n{}'.format(
            sns_message['fourCharacterId'], str(err), repr(err), xml_string))
        raise

    try:
        boto3.client('s3').put_object(
            Bucket=output_bucket_name, Key=site_log_filename_1, Body=site_log_data_1)

        logger.info('Site log XML for {} parsed to s3://{}/{}'.format(
            sns_message['fourCharacterId'], output_bucket_name, site_log_filename_1))

    except:
        logger.error('Failed to output Site Log data to s3://{}/{}'.format(
            output_bucket_name, site_log_filename_1))
        raise

    try:
        boto3.client('s3').put_object(
            Bucket=site_log_egress_bucket_name, Key=site_log_filename_2, Body=site_log_data_2)

        logger.info('Site log XML for {} parsed to s3://{}/{}'.format(
            sns_message['fourCharacterId'], site_log_egress_bucket_name, site_log_filename_2))

    except:
        logger.error('Failed to output Site Log data to s3://{}/{}'.format(
            site_log_egress_bucket_name, site_log_filename_2))
        raise

def lookup_password(key):
    ssm = session(os.environ['parameter_store_role_arn']).client('ssm')
    return ssm.get_parameter(Name=key, WithDecryption=True)['Parameter']['Value']

def session(role_arn):
    sts_client = boto3.client("sts")
    credentials = sts_client.assume_role(RoleArn=role_arn, RoleSessionName="upload-sitelog")['Credentials']
    return Session(aws_access_key_id=credentials['AccessKeyId'],
                    aws_secret_access_key=credentials['SecretAccessKey'],
                    aws_session_token=credentials['SessionToken'])

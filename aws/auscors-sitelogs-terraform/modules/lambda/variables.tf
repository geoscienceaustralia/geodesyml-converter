variable "region" {
  default = "ap-southeast-2"
}

variable "incoming_bucket_name" {
}

variable "incoming_bucket_arn" {
}

variable "bucket_name" {
}

variable "site_log_egress_bucket" {
  type = object({
    arn  = string
    name = string
  })
}

variable "sns_arn" {
}

variable "cognito_admin_auth_role_arn" {
}

variable "cognito_user_pool_id" {
}

variable "cognito_user_pool_client_id" {
}

variable "gws_url" {
}

variable "parameter_store_role_arn" {
}

variable "application" {
}

variable "environment" {
}

variable "owner" {
}

import boto3
import json
import logging
import os
import requests
import tempfile

from SiteLogToGeodesyML import sitelogtogeodesyml

def assume_role(role_arn: str):
    role = boto3.client('sts').assume_role(RoleArn=role_arn, RoleSessionName='ingest_text_site_log')
    credentials = role['Credentials']
    return boto3.session.Session(
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )

def lambda_handler(event, context):
    """
    Receive a text site log file from an S3:Put event, convert it to GeodesyML, and upload it to GWS.
    """

    text_site_log_file_name = None
    xml_site_log_file = None
    try:
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)

        s3_event = event['Records'][0]['s3']
        key = s3_event['object']['key']

        logger.info('Received {}'.format(key))

        bucket = s3_event['bucket']['name']
        version_id = s3_event['object']['versionId']

        gws_url = os.environ['gws_url']

        parameter_store_role_arn = os.environ['parameter_store_role_arn']
        gws_username = os.environ['gws_username']
        gws_password_key = os.environ['gws_password_key']
        cognito_admin_auth_role_arn = os.environ['cognito_admin_auth_role_arn']
        cognito_user_pool_id = os.environ['cognito_user_pool_id']
        cognito_user_pool_client_id = os.environ['cognito_user_pool_client_id']

        ssm = assume_role(parameter_store_role_arn).client('ssm')
        gws_password = ssm.get_parameter(Name=gws_password_key, WithDecryption=True)['Parameter']['Value']

        cognito = assume_role(cognito_admin_auth_role_arn).client('cognito-idp')
        response = cognito.admin_initiate_auth(
            AuthFlow='ADMIN_USER_PASSWORD_AUTH',
            AuthParameters={
                'USERNAME': gws_username,
                'PASSWORD': gws_password
            },
            UserPoolId=cognito_user_pool_id,
            ClientId=cognito_user_pool_client_id,
        )
        token = response['AuthenticationResult']['IdToken']

        obj = boto3.resource('s3').Object(bucket, key)
        text_site_log = obj.get(VersionId=version_id)['Body'].read()

        text_site_log_file_name = os.path.join(tempfile.gettempdir(), key)

        with open(text_site_log_file_name, 'w+b') as text_site_log_file:
            text_site_log_file.write(text_site_log)

        xml_site_log_file = tempfile.NamedTemporaryFile(delete=False)

        sitelogtogeodesyml.convert(text_site_log_file_name, xml_site_log_file.name)
        with open(xml_site_log_file.name, 'r+b') as xml_site_log_file:
            xml_site_log = xml_site_log_file.read()

        logger.info('Converted {} to xml: {}'.format(key, xml_site_log))

        headers = {'content-type': 'application/xml', 'Authorization':'Bearer ' + token}
        response = requests.post(gws_url + '/siteLogs/upload', data=xml_site_log, headers=headers)

        response.raise_for_status()

        logger.info('Uploaded generated xml to {}'.format(gws_url))
    except Exception as err:
        if text_site_log_file_name and os.path.exists(text_site_log_file_name):
            os.remove(text_site_log_file_name)

        if xml_site_log_file and os.path.exists(xml_site_log_file.name):
            os.remove(xml_site_log_file.name)

        raise

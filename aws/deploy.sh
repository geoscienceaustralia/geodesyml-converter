#!/usr/bin/env bash

set -e

while [[ $# -gt 0 ]]; do
    case $1 in
        -e|--env)
        environment="$2"
        shift
        shift
        ;;
        -d|--dry-run)
        dryRun=true
        shift
        ;;
        *)
        shift
        ;;
    esac
done

export TF_VAR_application=auscors-sitelogs
export TF_VAR_environment=${environment:-dev}
export TF_VAR_tf_state_bucket=geodesy-ops-terraform-state-$TF_VAR_environment
export TF_VAR_tf_state_table=terraform-state-lock

cd aws/auscors-sitelogs-terraform/

terraform init \
    -backend-config "bucket=${TF_VAR_tf_state_bucket}" \
    -backend-config "dynamodb_table=${TF_VAR_tf_state_table}" \
    -backend-config "region=ap-southeast-2" \
    -backend-config "key=$TF_VAR_application/terraform.tfstate"

terraform get
terraform plan -var-file="$TF_VAR_environment".tfvars

if [ -z "$dryRun" ]; then
    terraform apply -auto-approve -var-file="$TF_VAR_environment".tfvars
fi

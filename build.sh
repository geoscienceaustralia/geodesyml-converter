#!/usr/bin/env bash

set -euo pipefail

virtualenv python3-env && source ./python3-env/bin/activate

pip install -r requirements-dev.txt
pip install -r requirements.txt
pip install .

python -m pytest

cd aws/auscors-sitelogs-terraform/

./modules/lambda/create-deployment-package.sh
./modules/lambda/create-ingest-text-site-log-package.sh
./modules/lambda/create-fetch-site-logs-package.sh

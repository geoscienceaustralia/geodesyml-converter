#!/usr/bin/env bash

# Install nix in docker container

version=v0.9.0
arch=x86_64-linux

curl -sSf -L https://github.com/DeterminateSystems/nix-installer/releases/download/${version}/nix-installer-${arch} -o nix-installer
chmod +x nix-installer

# Override default user start uid (872415232), which is, for some reason, too high for docker in Bitbucket Pipelines.
./nix-installer install linux --extra-conf "sandbox = false" --extra-conf "start-id = 10100" --init none --no-confirm

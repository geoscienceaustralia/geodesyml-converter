import pytest
import os

from fetch_site_logs import gws_list_site_logs

def test_get_gws_site_logs():
    os.environ['gws_url'] = 'https://dev-metadata.gnss.ga.gov.au/api'
    sites = gws_list_site_logs()
    assert isinstance(sites, dict)
    # this can fail if sites are deleted from dev database for some reason
    assert len(sites) >= 1
